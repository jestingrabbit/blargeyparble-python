"""
Created on Apr 5, 2012

@author: bill
"""

import model
import math


def ellipses(maxradius=50, aspectratio=1, coeffofx2=None, coeffofy2=None, xoffset=0, yoffset=0):

    """creates a folder of pngs of all the ellipses with aspectratio"""

    dx = 2*int(maxradius) + 5

    if coeffofx2 is None and coeffofy2 is None:

        coeffofx2 = 1
        # coeffofy2 = aspectratio ** 2

        evalstr = "(x-" + str(xoffset) + ") ** 2 + (" + str(aspectratio**2) + " * (y-" + str(yoffset) + ")**2)"

    else:

        if coeffofx2 is None:
            coeffofx2 = 1

        if coeffofy2 is None:
            coeffofy2 = 1

        aspectratio = math.sqrt(float(coeffofy2) / coeffofx2)

        evalstr = "(" + str(coeffofx2) + " * (x-" + str(xoffset) + ")**2) + (" \
                  + str(coeffofy2) + " * (y-" + str(yoffset) + ")**2)"

    dy = 2 * int(float(maxradius)/aspectratio) + 5

    if aspectratio == 1:

        name = "circles"

    else:

        name = "ellipses"+str(aspectratio)
    
    if xoffset != 0:
        name = name + "xos" + str(xoffset)
    
    if yoffset != 0:
        name = name + "yos" + str(yoffset)

    palette = [(255, 255, 255), (0, 0, 0), (255, 0, 0)]

    m = model.Model(dx=dx,
                    dy=dy,
                    flat=True,
                    name=name,
                    palette=palette)

    def f(x, y):
        if (x % 5 == 0) and (y % 5 == 0):
            return 2
        else:
            return 0

    m.append_grid('b', f)
    g = m.grids[-1]    

    l = []

    i = m.i.iter_indices()

    mr2 = maxradius ** 2

    for (x, y) in i:

        v = eval(evalstr, {}, {'x': x, 'y': y})

        nv = v / coeffofx2

        if nv <= mr2:
            l.append((v, x, y))

    l.sort()

    i = 0

    while i < len(l):

        v0 = l[i][0]

        while l[i][0] == v0:

            (v, x, y) = l[i]
            g.put(1, x, y)
            n = [a for a in m.i.neighbours(x, y) if g.get(*a) == 1]

            if len(n) > 2:

                if (x-1, y-1) in n:
                    
                    corner = (x-1, y-1)

                elif (x-1, y+1) in n:
                    
                    corner = (x-1, y+1)

                elif (x+1, y-1) in n:
                    
                    corner = (x+1, y-1)

                else:
                    #(x+1, y+1) in n:
                    
                    corner = (x+1, y+1)

                n = [a for a in n if a in m.i.aneighbours(*corner)]

                c = n[0]

                g.put(f(*c), *c)

            i += 1

            if i == len(l):
                break

        m.make_plan(name="r"+str(v0).zfill(5), useolddir=True)


def squaring_the_circle(maxradius=50):
    """
    is squaring the circle in minecraft possible?
    """

    dx = 2*int(maxradius) + 5
    dy = dx

    palette = [(255, 255, 255), (0, 0, 0), (255, 0, 0)]

    m = model.Model(dx=dx,
                    dy=dy,
                    flat=True,
                    name="squaring_the_circle",
                    palette=palette)

    m.append_grid('i', "x**2 + y**2")
    g = m.grids[-1]

    mr2 = int(maxradius**2)
    l = [0]*mr2

    i = m.i.iter_indices()

    for (x, y) in i:
        v = g[x, y]
        if v < mr2:
            l[v] += 1

    odd_squares = [(2*n + 1)**2 for n in range(int(maxradius))]

    r = 0
    n = 0
    area = 0

    while r < len(l):

        while l[r] == 0 and r < len(l):
            r += 1
            if r == len(l):
                break

        if r == len(l):
            break

        area += l[r]
        r += 1

        while area > odd_squares[n]:
            n += 1

        if area == odd_squares[n]:

            m.append_grid('b', "0")
            p = m.last_grid()

            for (x, y) in m:
                if g[x, y] < r:
                    p[x, y] = 2

            m.append_hollowed_grid()
            p = m.last_grid()

            for t in range(n):
                p[n, n-t] = 1
                p[-n, t-n] = 1
                p[n-t, -n] = 1
                p[t-n, n] = 1

                if p[n, -t] == 0:
                    p[n, -t] = 1
                    p[-n, t] = 1
                    p[-t, -n] = 1
                    p[t, n] = 1

            m.make_plan(name="size" + str(odd_squares[n]), useolddir=True)

            m.grids = [m.grids[0]]

    dx += 1
    dy += 1

    m = model.Model(dx=dx,
                    dy=dy,
                    flat=True,
                    name="squaring_the_circle",
                    palette=palette)

    m.append_grid('f', "(x-0.5)**2 + (y-0.5)**2")
    g = m.grids[-1]

    l = [0]*mr2

    i = m.i.iter_indices()

    for (x, y) in i:
        v = g[x, y]
        if v < mr2:
            l[int(v)] += 1

    even_squares = [4*n**2 for n in range(1, int(maxradius))]

    r = 0
    n = 0
    area = 0

    while r < len(l):

        while l[r] == 0 and r < len(l):
            r += 1
            if r == len(l):
                break

        if r == len(l):
            break

        area += l[r]
        r += 1

        while area > even_squares[n]:
            n += 1

        if area == even_squares[n]:

            m.append_grid('b', "0")
            p = m.last_grid()

            for (x, y) in m:
                if g[x, y] < r:
                    p[x, y] = 2

            m.append_hollowed_grid()
            p = m.last_grid()

            for t in range(n+1):
                p[n+1, n+1-t] = 1
                p[-n, t-n] = 1
                p[n+1-t, -n] = 1
                p[t-n, n+1] = 1

                if p[n+1, -t] == 0 and t<n:
                    p[n+1, -t] = 1
                    p[-n, t+1] = 1
                    p[-t, -n] = 1
                    p[t+1, n+1] = 1

            m.make_plan(name="size" + str(even_squares[n]), useolddir=True)

            m.grids = [m.grids[0]]
