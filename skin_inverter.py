"""
a simple colour inverter, using pypng, for a black rabbit.
"""

__author__ = 'bill'

import png
import array

def inverter(skin = "rabbit-skin.png"):

    f = open(skin, 'rb')
    r = png.Reader(file=f)
    data = r.asDirect()

    width, height, pixels = data[0], data[1], data[2]

    pixels = list(pixels)
    pixels = [array.array('i', row) for row in pixels]

    planes = len(pixels[0])/width

    for i in range(height):
        for j in range(width):

            n = j*planes

            for offset in range(3):
                pixels[i][n+offset] = 255 - pixels[i][n+offset]

    w = png.Writer(width, height, alpha = True, bitdepth = 8)

    name = "new" + skin
    g = open(name, 'wb')
    w.write(g, pixels)
    g.close()
    f.close()