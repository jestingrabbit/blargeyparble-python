'''
Created on Sep 14, 2012

@author: bill
'''

import noise, model, ghash

def test2D(n=500):
    
    test = model.Model(dx=n, dy=n, cx=n/2, cy=n/2, flat = True, name = "noisetest")
    
    hasher = ghash.Ghasher(2)
    
    def noisefunc(x,y):
        return noise.BCCnoise2(x/250.0 , y/250.0, hasher)
    
    test.append_grid("f", noisefunc)
    datagrid = test.grids[0]
    
    minval = 0
    maxval = 0
    
    for index in test:
        
        v = datagrid[index]
        
        if v < minval:
            
            minval = v
        
        elif v > maxval:
            
            maxval = v
            
    print(("minval =", minval))
    print(("maxval =", maxval))
    
    spread = maxval - minval
    
    def f(x, y):
        """ value to palette value """
        return int(((datagrid[x,y] - minval)/spread)*255)
    
    test.append_grid('b', f)

    palette = [ (i, i, i) for i in range(255)]
    test.set_palette(palette)
    
    test.make_plan()

test2D()



def test3Dnaive(n=500):
    
    test = model.Model(dx=n, dy=n, cx=n/2, cy=n/2, 
                       flat = True, 
                       name = "noisetest3D")
    
    hasher = ghash.Ghasher(3)
    
    def noisefunc(x,y):
        return noise.BCCnoise3(x/250.0 , y/250.0, 0, hasher)
    
    test.append_grid("f", noisefunc)
    datagrid = test.grids[0]
    
    minval = 0
    maxval = 0
    
    for index in test:
        
        v = datagrid[index]
        
        if v < minval:
            
            minval = v
        
        elif v > maxval:
            
            maxval = v
            
    print(("minval =", minval))
    print(("maxval =", maxval))
    
    spread = maxval - minval
    
    def f(x, y):
        """ value to palette value """
        return int(((datagrid[x,y] - minval)/spread)*255)
    
    test.append_grid('b', f)

    palette = [ (i, i, i) for i in range(255)]
    test.set_palette(palette)
    
    test.make_plan()

test3Dnaive()


