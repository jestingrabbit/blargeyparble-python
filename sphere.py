"""
outputs blueprints for a sphere.
"""

__author__ = 'bill'

import model
import math

def sphere(radius2, blockcentred = True):

    r = int(math.sqrt(radius2))

    if blockcentred:

        dx = 2*r + 5
        dy = dx
        dz = dx
        evalstr = "x**2 + y**2 + z**2"

    else:

        radius2 += 0.25

        dx = 2*r + 6
        dy = dx
        dz = dx
        evalstr = "(x+0.5)**2 + (y+0.5)**2 + (z+0.5)**2"

    m = model.Model(dx=dx,
                    dy=dy,
                    dz=dz,
                    name="sphere" + str(radius2),
                    palette=[(255, 255, 255), (0, 0, 0), (85, 85, 85), (170, 170, 170), (255, 0, 0)])

    m.append_grid('f', evalstr)
    g = m.last_grid()
    m.append_grid('b', "0")
    h = m.last_grid()

    for x, y, z in m:
        if g[x, y, z] <= radius2:
            h[x, y, z] = 1

    m.append_hollowed_grid()
    g = m.last_grid()
    m.append_grid('b', "0")
    h = m.last_grid()

    for z0 in m.i.range_z()[1:]:
        for (x, y, z) in m.i.iter_indices_z_level(z0):
            if g[x, y, z] == 0 and g[x, y, z-1] == 1:
                # pale grey for "empty this level, full below"
                h[x, y, z] = 3
            elif g[x, y, z] == 1 and g[x, y, z-1] == 0:
                # dark grey for "full this level, empty below"
                h[x, y, z] = 2
            elif g[x, y, z] == 1 and g[x, y, z-1] == 1:
                # black for "put a block on a block"
                h[x, y, z] = 1

    m.add_torches()
    m.make_plan()
