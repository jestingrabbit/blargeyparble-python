from math import floor

def linear2D(x, y, a):
    ix = floor(x)
    iy = floor(y)
    fx = x-ix
    fy = y-iy

    a00 = a[ix, iy]
    a01 = a[ix, iy+1]
    a10 = a[ix+1, iy]
    a11 = a[ix+1, iy+1]
    
    return fx*(fy*a00 + (1-fy)*a01) + (1-fx)*(fy*a10 + (1-fy)*a11)