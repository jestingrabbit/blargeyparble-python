from math import sqrt, atan, sin, floor


coords = [(35, 0),(35, 1),(35, 2),(35, 3),(35, 4),(35, 5),
(34, 6),(34, 7),(34, 8),(34, 9),(34, 10),
(33, 11),(33, 12),(33, 13), 
(32, 14),(32, 15),
(31, 16),(31, 17),
(30, 18),
(29, 19),(29, 20),
(28, 21),
(27, 22),(27, 23),
(26, 24),
(25, 25)]


def round(x):
    return int(floor(x+0.5))


def altitude(x, y):
    r= sqrt(x**2 + y**2)
    return round(0.5*r*sin(2*atan(y/x)))
    

def gimme(c):    
    for (x, y) in c:
        print(x, y, altitude(x, y))

def f():
    return 20
