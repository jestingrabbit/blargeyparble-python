"""
For mineopticon. A cylindrical wall that has a design from a png on it.
"""

__author__ = 'bill'


import math
import model
import png
import array



def png_wall_design(pngdesign,
                    radius2,
                    blockcentred=True,
                    blank=0.25):

    r = math.sqrt(radius2)
    rint = int(r)
    pi = math.pi

    if blockcentred:
        dx = 2*rint + 5
    else:
        dx = 2*rint + 6

    dy = dx

    virtual_circumference = 2*pi*r
    used_circumference = virtual_circumference * (1-blank)

    f = open(pngdesign, 'rb')
    r=png.Reader(file=f)
    designdata = r.asDirect()

    designwidth, designheight, pixels = designdata[0], designdata[1], designdata[2]

    pixels = list(pixels)
    pixels = [array.array('i', row) for row in pixels]

    designplanes = len(pixels[0])/designwidth

    dz = int(designheight*used_circumference/designwidth) + 2
    cz = dz-1

    name = pngdesign[0: len(pngdesign)-4]

    m = model.Model(dx=dx,
                    dy=dy,
                    dz=dz,
                    cz=cz,
                    name=name,
                    palette=[(255, 255, 255),
                             (170, 170, 0), (255, 255, 85),
                             (170, 0, 170), (255, 85, 255),
                             (255, 0, 0)])


    m.append_grid('b', "0")
    g = m.last_grid()

    for x, y, z in m:
        if not blockcentred:
            x0 = x + 0.5
            y0 = x + 0.5
        else:
            x0 = x
            y0 = y

        if x0**2 + y0**2 <= radius2:
            g[x, y, z] = 1

    m.append_hollowed_grid()
    g = m.last_grid()


    def pixeldata(row, col):

        values = pixels[row][col*designplanes : (col+1)*designplanes]

        if sum(values)/designplanes > 127:
            return 0
        else:
            return 1


    def grid_voxel_to_png_pixel_value(x, y, z):

        if not blockcentred:
            x += 0.5
            y += 0.5

        angle = math.atan2(y, x)

        if math.fabs(angle) >= pi*(1-blank) or z == 0 or z == 1-dz:
            return 0
        else:
            column = int((angle/(2*pi*(1-blank)) + 0.5) * designwidth)
            row = int( (z+0.5)*designheight/(2.0-dz) )
            return pixeldata(row, column)


    for x, y, z in m:

        if g[x, y, z] != 0:
            g[x, y, z] += 2 * grid_voxel_to_png_pixel_value(x, y, z)

    m.append_grid('b', "0")
    h = m.last_grid()

    sand = 0
    gravel = 0

    for x, y, z in m:
        if g[x, y, z] == 1:
            sand += 1
        elif g[x, y, z] == 3:
            gravel += 1

        if z != 1-dz:
            if g[x, y, z] == g[x, y, z-1]:
                h[x, y, z] = g[x, y, z]
            else:
                h[x, y, z] = g[x, y, z] + 1
        else:
            h[x, y, z] = g[x, y, z]

    m.add_torches()

    print("sand =", sand)
    print("gravel =", gravel)

    m.make_plan()