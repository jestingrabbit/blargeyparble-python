'''
Created on Sep 25, 2012

@author: bill
'''

import model, math

def ufo( radius = 9 ):
    
    R = radius ** 2
    
    d = int(math.ceil(radius) * 2 + 2)
    m = model.Model( dx= d, dy = d, dz = d + 6, cx = 0, cy = 0, cz = (2*d)/3 + 3, name = "ufo" )

    c = (d-1)/2.0
    
    def f(x, y, z):
        
        xnew = (x - c)/radius
        ynew = (y - c)/radius
        
        r2 = xnew**2 + ynew**2
        
        znew = 2.5*z/radius + r2
        
        v = r2 + znew**2
       
        return int( v < 1 )
    
    m.append_grid('b', f)
    
    m.append_hollowed_grid()
    
    this_grid = m.last_grid()

    height = m.i.min_z()
    
    while this_grid[d/2 - 2, d/2 - 1, height] == 0:
        height += 1
    
    floorheight = height
    
    if this_grid[d/2 - 1, d/2 - 1, height] == 0:
        height += 1
    
    this_grid[d/2 - 1, d/2 - 1, height] = 0
    this_grid[d/2    , d/2 - 1, height] = 0
    this_grid[d/2 - 1, d/2    , height] = 0
    this_grid[d/2    , d/2    , height] = 0
    
    for x, y, z in this_grid:
        if z > floorheight and this_grid[x,y,z] != 0:
            this_grid[x,y,z] = 2
    
    for x, y, z in this_grid.i.iter_indices_z_level( floorheight ):    
        if this_grid[x, y, z] != 0 and (x-c)**2 + (y-c)**2 > R/2:
            this_grid[x, y, z] = 3
    
    design = model.Model(dx = m.i.dx, dy = m.i.dy, cx = 0, cy = 0, flat = True, name = "design", palette = [(255, 255, 255), (0,0,0)])
    
    b = (d/2 - 1.5 - (d/2 -1)/2.0)**2
    
    def g(x, y):
        return (x- (d/2 - 0.5))**2 + (y- (d/2 -1)/2.0)**2 < b
    
    design.append_grid('b', g)
    design.append_hollowed_grid()
    
    for x, y in design:
        design.last_grid()[x, y] = max(design.last_grid()[x, y], design.last_grid()[x, d - 1 - y])
    for x, y in design:
        design.last_grid()[x, y] = max(design.last_grid()[x, y], design.last_grid()[y, x])
    
    design.make_plan()

    for x, y, z in m:
        if design.last_grid()[x, y] != 0 and this_grid[x, y, z] == 1:
            this_grid[x, y, z] = 4
    
    block_count = [0,0,0,0,0]
    
    for x, y, z in m:
        block_count[this_grid[x,y,z]] += 1
        
    print("lapis = ", block_count[1])
    print("glass = ", block_count[2])
    print("glowstone = ", block_count[3])
    print("gold = ", block_count[4])
    
    palette = [(255, 255, 255), (0,0,255), (0, 255, 0), (255, 0, 0), (255, 255, 0)]
    
    m.set_palette(palette)
    m.make_plan()

ufo()        
            
            
        
        
    
    
    
    
    
    
    
    
            
    