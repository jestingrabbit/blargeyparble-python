__author__ = 'bill'

from random import sample

def doo_eet():

    range1 = list(range(0, 56, 2))
    range2 = list(range(1, 56, 2))

    choose1 = sample(range1, 8)
    choose2 = sample(range2, 4)

    sequence1 = list(zip(choose1, ['B1', 'G1', 'R1', 'Y1', 'B1', 'G1', 'R1', 'Y1']))
    sequence2 = list(zip(choose2, ['B2', 'G2', 'R2', 'Y2']))

    sequence = sorted(sequence1 + sequence2)

    print(sequence)