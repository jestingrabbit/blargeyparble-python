__author__ = 'bill'

import operator


def checksorted(alist, comparison=operator.le):
    return all(comparison(alist[i], alist[i + 1]) for i in range(len(alist) - 1))
