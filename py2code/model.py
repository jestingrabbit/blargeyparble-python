'''
Created on Apr 5, 2012

@author: bill
'''

import sys
import array
import itertools
import math
import os
import datetime
import png

eval_globals = math.__dict__


class Indices:

    '''Index class for grids and models to use. The user will only need to define
    the index once per (simple) Model. Mainly used as a way to generate an
    iterator that outputs (x, y, z) values that are in a particular rectangular
    prism with dimensions dx, dy, dz, starting (-cx, -cy, -cz). The first dx*dy
    indexes are all from the z=0 layer of the prism. Used to iterate through a
    Grid.'''

    def __init__(self,
                 dx=0, dy=0, dz=0,
                 cx=None, cy=None, cz=None,
                 flat = False):
        '''-cx<= x < dx-cx, also true for other coords. flat <=> 2 dimensional.
        d[iameter], c[entre]. length = dx*dy[*dz].'''

        self.dx = dx
        if cx != None:
            self.cx = cx
        else:
            self.cx = dx/2
            
        self.dy = dy
        if cy != None:
            self.cy = cy
        else:
            self.cy = dy/2

        self.flat = flat

        if flat:
            self.length = dx*dy

        else:
            self.dz = dz
            if cz != None:
                self.cz = cz
            else:
                self.cz = dz/2

            self.dxdy = dx * dy

            self.length = dx*dy*dz


    def iter_indices(self):
        '''Outputs an iterator that ranges over all
        acceptable index values (x, y[, z])'''

        if self.flat:
            return itertools.imap(reversed, itertools.product(
                range(-self.cy, self.dy - self.cy),
                range(-self.cx, self.dx - self.cx)))

        else:
            return itertools.imap(reversed, itertools.product(
                range(-self.cz, self.dz - self.cz),
                range(-self.cy, self.dy - self.cy),
                range(-self.cx, self.dx - self.cx)))

    def range_x(self):

        return range(-self.cx, self.dx - self.cx)
    
    def min_x(self):
        
        return -self.cx

    def max_x(self):
        
        return self.dx -self.cx -1
    
    def range_y(self):

        return range(-self.cy, self.dy - self.cy)

    def min_y(self):
        
        return -self.cy

    def max_y(self):
        
        return self.dy -self.cy -1
    
    def range_z(self):

        return range(-self.cz, self.dz - self.cz)

    def min_z(self):
        
        return -self.cz

    def max_z(self):
        
        return self.dz -self.cz -1
    

    


    def iter_indices_z_level(self, z):
        '''Outputs an iterator that ranges over all
        acceptable index values (x, y[, z]) for some fixed z.'''


        if self.flat:
            return itertools.imap(reversed, itertools.product(
                range(-self.cy, self.dy - self.cy),
                range(-self.cx, self.dx - self.cx)))
        
        else:
            return itertools.imap(reversed, itertools.product(
                [z],
                range(-self.cy, self.dy - self.cy),
                range(-self.cx, self.dx - self.cx)))



    def i2a(self, *coords):
        '''index values, (x, y[, z]) are converted to an array index value.
        0<=i2a(x, y, z)<length'''

        

        if not self.is_indexed(*coords):
            raise IndexError("Grid index out of range (i2a)")

        x, y = coords[:2]

        x0 = x + self.cx
        y0 = y + self.cy
        
        if self.flat:            
            return y0 * self.dx + x0
        else:
            z = coords[2]
            z0 = z + self.cz
            return z0*self.dxdy + y0*self.dx + x0
            
        

    def a2i(self, n):
        '''function inverse of i2a.'''

        if not (0 <= n < self.length):
            raise IndexError("array index out of range (a2i)")            

        x = (n % self.dx) - self.cx
        m = n/self.dx
        y = (m % self.dy) - self.cy

        if self.flat:
            return x, y

        else:
            z = m/self.dy - self.cz
            return x, y, z

    def is_indexed(self, *coords):
        '''Makes sure coords is in the indexed rectangular prism.'''

        if ((self.flat and len(coords) != 2) or
            (not self.flat and len(coords) != 3)):
            return False

        elif self.flat:
            return ((-self.cx <= coords[0] < self.dx-self.cx) and
                    (-self.cy <= coords[1] < self.dy-self.cy))

        else:
            return ((-self.cx <= coords[0] < self.dx-self.cx) and
                    (-self.cy <= coords[1] < self.dy-self.cy) and
                    (-self.cz <= coords[2] < self.dz-self.cz))

    def aneighbours(self, *coords):
        '''returns iterator over the 6 (or 4 if flat) neighbors a voxel has.'''

        if self.flat:
            x, y = coords
            a = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]

        else:
            x, y, z = coords
            a = [(x-1, y, z), (x+1, y, z), (x, y-1, z), (x, y+1, z),
                 (x, y, z-1), (x, y, z+1)]

        return [index for index in a if self.is_indexed(*index)]


    def neighbours(self, *coords):

        if self.flat:

            x, y = coords

            a = [(x-1, y), (x+1, y), (x, y-1), (x, y+1),
                 (x-1, y-1), (x+1, y+1), (x+1, y-1), (x-1, y+1)]

        else:

            x, y, z = coords

            a = [(x-1, y,z), (x+1, y,z), (x, y-1,z), (x, y+1,z),
                 (x-1, y-1,z), (x+1, y+1,z), (x+1, y-1,z), (x-1, y+1,z),
                 (x-1, y,z+1), (x+1, y,z+1), (x, y-1,z+1), (x, y+1,z+1), (x, y, z+1),
                 (x-1, y-1,z+1), (x+1, y+1,z+1), (x+1, y-1,z+1), (x-1, y+1,z+1),
                 (x-1, y,z-1), (x+1, y,z-1), (x, y-1,z-1), (x, y+1,z-1), (x, y, z-1),
                 (x-1, y-1,z-1), (x+1, y+1,z-1), (x+1, y-1,z-1), (x-1, y+1,z-1)]
                 

        return [index for index in a if self.is_indexed(*index)]



def testindices(dx = 15, dy = 13, dz = 11, cx=0, cy= -3, cz = 27):

    i = Indices(dx, dy, dz, cx, cy, cz)

    a = itertools.starmap(lambda x, y: x == y,
                          itertools.izip_longest(
                              range(i.length),
                              itertools.starmap(i.i2a, i.iter_indices())))

    return all(a)

    


class GridError(Exception):

    def __init__(self, indexlength, arraylength):

        Exception.__init__(self)
        self.args = (indexlength, arraylength)

        print "indexlength=", indexlength,", arraylength=", arraylength,". This is not good."



class Grid:
    '''turns an array into a 3d or 2d array.'''

    def __init__(self,
                 gridindex = None,
                 dx=0, dy=0, dz=0,
                 cx=None, cy=None, cz=None,
                 flat=False, typecode='b', init = ''):
        ''' self.i = index, self.typecode = f(loat), i(nt), b(lock).
        self.data = an array of typecode values, one per index.
        init should be a function from x, y, z to typecode values, or
        a string that evaluates as typecode, when given index values
        for (x, y, z).'''

        if gridindex is None:
            self.i = Indices(dx, dy, dz, cx, cy, cz, flat)
        else:
            self.i = gridindex


        self.typecode = typecode

        
        if typecode == 'f':
            if sys.float_info.mant_dig == 53:
                self.atc = 'd'
            else:
                self.atc = 'f'

        elif typecode == 'i':
            self.atc = 'i'
            
        elif typecode == 'b':
            self.atc = 'B'


        try:

            if self.i.flat:

                self.data = array.array(self.atc,
                    itertools.starmap(lambda x, y: eval(init, eval_globals,
                                    {'x':x, 'y':y}),
                  self.i.iter_indices()))

            else:


                
                self.data = array.array(self.atc,
itertools.starmap(lambda x, y, z: eval(init, eval_globals,
                                       {'x':x, 'y':y, 'z':z}),
                  self.i.iter_indices()))

        except SyntaxError:

            if init == '':
                self.data = array.array(self.atc, self.i.length * [0])

            else:
                raise

        except TypeError:

            if callable(init):
                self.data = array.array(self.atc,
itertools.starmap(init, self.i.iter_indices()))

        except:
            self.data = array.array(self.atc, init)

        if self.i.length != len(self.data):

            raise GridError(self.i.length, self.data)
    
    def __len__(self):
        return self.i.length

    def get(self, *coords):
        ''' get the Grid value at *coords.'''
        return self.data[self.i.i2a(*coords)]
    
    def __getitem__(self, coords):
        return self.data[self.i.i2a(*coords)]

    def put(self, value, *coords):
        ''' put value at (x, y[, z]).'''
        self.data[self.i.i2a(*coords)] = value
    
    def __setitem__(self, coords, value):
        self.data[self.i.i2a(*coords)] = value
    
    def __iter__(self):
        return self.i.iter_indices()
    
    def iterkeys(self):
        return self.i.iter_indices()
    
    def __contains__(self, coords):
        return self.i.is_indexed(*coords)

    def neighbour_values(self, *coords):
        '''a list of the 6 (or 4 if flat) Grid values.'''
        return [self.get(*c) for c in self.i.aneighbours(*coords)]

    def visible(self, *coords):
        """return Grid.get(*coords) if the cell is occupied (non zero) and adjacent to an empty cell, else return 0"""

        value = self.get(*coords)

        if value:
            if not all(self.neighbour_values(*coords)):
                return value
            else:
                return 0
        else:
            return 0
        



class Model:
    '''A bag for an index (maybe free that up later, different Indices might be
    a plus), and a list of grids. Plus methods that move from one Grid to the
    next ie hollow stuff out, put a layer of foo on a layer of bar. Plus a
    palette and a method to output the Model to pngs, one per layer, and, maybe
    later, one per view.'''

    def __init__(self,
                 i = None,
                 dx=0, dy=0, dz=0,
                 cx=None, cy=None, cz=None,
                 flat = False,
                 name = "",
                 palette = []):

        if i == None:
            self.i = Indices(dx, dy, dz, cx, cy, cz, flat)

        else:
            self.i = i

        self.grids = []

        self.name = name

        self.palette = palette

    def set_name(self, name = ""):

        """define the name of the Model, used for making directories for output"""

        self.name = name

    def set_indices(self, i = None,
                 dx=0, dy=0, dz=0,
                 cx=0, cy=0, cz=0,
                 flat = False):
        """put the index for the Model"""
        

        if i is None:
            self.i = Indices(dx, dy, dz, cx, cy, cz, flat)

        else:
            self.i = i

    def append_grid(self, typecode, init):

        """append a Grid to the list of grids"""
        
        self.grids.append(Grid
                          (gridindex = self.i,
                           typecode = typecode,
                           init = init))

    def last_grid(self):
        return self.grids[-1]

    def append_conditional_blocks_grid(self, condition= "False", entry = -1):

        """append a type 'b' Grid of eval(condition, math.__dict__, {'g': egrid.get(*coords)})"""

        egrid = self.grids[entry]
        initcondition = lambda *coords: eval(condition, eval_globals, {'g': egrid.get(*coords)})
        self.append_grid( typecode = 'b', init = initcondition)

    def append_nonneg_blocks_grid(self):
        self.append_conditional_blocks_grid(condition = "g>=0")

    def append_positive_blocks_grid(self):
        self.append_conditional_blocks_grid(condition = "g>0")
  
    def append_hollowed_grid(self, entry = -1):

        """append a block Grid, removing blocks which are invisible to the observer"""

        egrid = self.grids[entry]

        self.append_grid( typecode = 'b', init = egrid.visible)

    def append_masked_grid(self, mask, entry = -1):

        """mask is a bool when evaled or called on *coords"""

        egrid = self.grids[entry]

        if callable(mask):

            def init(*coords):
                if mask(*coords):
                    return egrid.get(*coords)
                else:
                    return 0
                          
        else:

            def init(*coords):
                
                eval_locals = {'x':coords[0], 'y': coords[1]}
                
                if not self.i.flat:
                    eval_locals.update({'z':coords[2]})
                
                if eval(mask, eval_globals, eval_locals):
                    return egrid.get(*coords)
                else:
                    return 0
                              
        self.append_grid( typecode = 'b', init = init)


    def __iter__(self):
        return self.i.iter_indices()
    
    def iterkeys(self):
        return self.i.iter_indices()
    
    def __contains__(self, coords):
        return self.i.is_indexed(*coords)

    def add_torches(self, torch_value = None, spacing = 5):

        if torch_value is None:
            torch_value= len(self.palette) - 1

        g = self.last_grid()

        for coords in self:
            if self.i.flat:
                x, y = coords
                if x%5 == 0 and y%5 == 0 and g[x, y] == 0:
                    g[x, y] = torch_value
            else:
                x, y, z = coords
                if x % 5 == 0 and y % 5 == 0 and g[x, y, z] == 0:
                    g[x, y, z] = torch_value




    def set_palette(self, palette):

        """define a palette, used for making plans."""

        self.palette = palette

    def make_plan(self, entry = -1, name = None, useolddir = False, zrange = None):

        """should write a bunch of pngs that show you what to build"""
        
        try:
            os.mkdir(self.name)
        except OSError:
            if not useolddir:
                self.name = self.name + datetime.datetime.today().isoformat()
                os.mkdir(self.name)

        Grid = self.grids[entry]

        w = png.Writer(Grid.i.dx, Grid.i.dy, palette = self.palette, bitdepth = 8)

        if Grid.i.flat:

            try:
                f = open(os.path.join(self.name, name+'.png'), 'wb')
            except:
                f = open(os.path.join(self.name, "view"), 'wb')
                
            w.write(f, w.array_scanlines(Grid.data))
            f.close()

        else:

            if zrange is None:
                zrange = range(self.i.dz)

            for z in zrange:
                f = open(os.path.join(self.name, 'lvl'+str(z).zfill(3))+'.png', 'wb')
                low = Grid.i.dxdy * z
                high = Grid.i.dxdy * (z+1)
                w.write(f, w.array_scanlines(Grid.data[low:high]))
                f.close()

                
                






