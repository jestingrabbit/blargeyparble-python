# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 01:57:47 2012

Not a great implementation of the kind of hasher that is found in Gustavson.
"""

from math import factorial as factorial
import random


def twice_number_of_gradients( n ):
    """
    A quick way to work out the base of a modulus that we can use.
    """
    if n == 2:
        return 32    
    elif n == 3:
        return 24
    elif n== 4:
        return 64
    else:
        zeroes = n//3
        return 2**(n-zeroes)*factorial(n)/(
                                        factorial(n-zeroes)*factorial(zeroes))



class Ghasher(object):
    """
    Used to hash a vector of integers.
    """
    
    def __init__(self, n, state = None):
        """
        n is the dimension, state should be something returned by 
        self.get_initial_state
        """
        self.dim = n
        modulus = twice_number_of_gradients( n )
        self.length = (512//modulus)*modulus
        
        array = []
        
        if state == None:
            random.seed()
            self.initialstate = random.getstate()
        else:
            random.setstate(state)
            self.initialstate = state
        
        for i in xrange(self.length):
            array.insert( random.randrange(i+1) , i )
        
        self.array = array
        self.modarray = [ i % modulus for i in array]

    def hash(self, coords ):
        """
        calculates the hash of an integer value vector. Could be cleaned up.
        All these '%'s are dumb...
        """
        index = 0
        for i in range(self.dim - 1):
            index = (coords[i] + index) % self.length       
            index = self.array[index]
        
        return self.modarray[ (index + coords[self.dim - 1]) % self.length ]

    def get_initial_state(self):
        """
        don't use this, just use the data method. Good for regenerating the
        same hasher.
        """        
        return self.initialstate
            
            
            
        
        
        