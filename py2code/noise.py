from math import sqrt, cos, sin, pi
import itertools

# This is hopefully a clear description of a 
# somewhat faster (at least in higher dimensions)
# way to make differentiable noise.

# Basically, it uses the same techniques as simplex 
# noise ie in n dimensions the noise is calculated
# by summing n+1 differentiable and randomised bumps. 
# However, instead of spherical bumps centred at
# simplex vertices, we use bumps that are non zero on a 
# region composed of two adjacent simplexes. 
# These bump functions are the product of n+2 barycentric
# coordinates, and, by reusing multiplications, there are 
# oportunities to make significant savings in arithmetic
# operations over the simplex noise algorithm. I can prove 
# that we need only linear multiplications if we allow
# ourselves n divisions (which is kinda useless, but it 
# demonstrates an upper bound). This compares favourably
# with the n^2 + O(n) multiplications needed by simplex noise.

# Two dimensions perhaps isn't the best place to write
# this algorithm, and the particular implementation here
# is suboptimal (especially because I use python's native
# hash function), but it should at least allow you to
# determine whether the output is fit for purpose, which
# is largely the reason that I'm sharing it with you.

# nb: At one point I refer to Gustavson, by which I mean
# the paper by Stefan Gustavson, "Simplex Noise Demystified"
# and I specifically am referring to the hashing method in
# that paper. Its a good paper but it suggests that
# simplex noise has linear time complexity. However, calculating
# n+1 distances requires n*(n+1) multiplications, making it
# quadratic in time. That an algorithm of this kind has this
# complexity class is, I believe, unavoidable. We need hashes
# of n+1 points in n-dimensional space, which implies O(n^2)
# (just to calculate the vectors that needs to be hashed). However,
# most of the cycles are going to be spent on multiplications, 
# which this algorithm tries hard to reduce, and does in higher
# dimensions.


A2 = -0.5 + 0.5*sqrt(1/3.0)
B2 = 0.5*(sqrt(3) -1)

onethird = 1.0/3
twothirds = 2.0/3

eighth = 1.0 / 8
gradient2D = [ ( cos( pi * eighth * i ), sin( pi * eighth * i ) ) 
                    for i in range(-8, 8) ]

def dot2D((a,b), (c,d)):
    """
    just an explicit dot product for 2D space.
    """
    return a*c + b*d


def BCCnoise2(x, y, hasher):
    """
    BaryCentric Coordinates noise in two dimensions. Not more efficient 
    here, but it shouldn't be much less efficient either.
    """
    
    # first we need to stretch the vector in the direction of (1,1) to place it
    # into the rectilinear simplex grid.
    # v is for value. r is for rectilinear.
    
    v = B2*(x+y)
    rx = x + v
    ry = y + v
    
    # Now we need the integer (i) and fractional (f) parts of our variables.
    
    if rx >= 0:
        ix = int(rx)
    else:
        ix = int(rx) - 1
    
    if ry >= 0:
        iy = int(ry)
    else:
        iy = int(ry) - 1
    
    fx = rx - ix
    fy = ry - iy

    # Need to define the relevant barycentric coords. ai's are in the "home" 
    # simplex and bi's are the barycentric coords in the neighbouring 
    # simplexes that we require.
    
    if fx >= fy:
        
        largex = True
        
        a0 = fy
        a1 = fx - fy
        a2 = 1 - fx
        b0 = 1 - fy
        b1 = 1 - fx + fy
        b2 = fx
        
    elif fx < fy:
        
        largex = False
       
        a0 = fx
        a1 = fy - fx
        a2 = 1 - fy
        b0 = 1 - fx
        b1 = 1 - fy + fx
        b2 = fy
    
    # the bump functions are products of barycentric coordinates,
    # but there is a smart way to calculate them, different for every 
    # dimension, and providing more savings vs simplex in higher dimensions.
    
    a0b0 = a0 * b0
    a1b1 = a1 * b1
    a2b2 = a2 * b2
    
    bump0 = a0b0 * a1b1
    bump1 = a1b1 * a2b2
    bump2 = a2b2 * a0b0
    
    smoothbump0 = bump0 * bump0
    smoothbump1 = bump1 * bump1
    smoothbump2 = bump2 * bump2  
    
    # We need the rectilinear coordinates of the centres 
    # of the sides of the simplex that our point is in,
    # so that we have a unique thing to hash for each bump.
    # We double the values to ensure we're dealing with integers.
    # r is for rectilinear coordinate grid.

    ix2 = 2*ix
    iy2 = 2*iy

    if largex:
        rcentre0 = (ix2+2, iy2+1)
        rcentre1 = (ix2+1, iy2  )
        rcentre2 = (ix2+1, iy2+1)
    else:
        rcentre0 = (ix2+1, iy2+2)
        rcentre1 = (ix2  , iy2+1)
        rcentre2 = (ix2+1, iy2+1)
    
    # We need a hash value for each centre, to get a random gradient.
    # An important technicality: making sure that the hashed object is the same
    # on both sides of the simplex face is critical, so
    # myhash(midpoint((i, j), (i+1, j))) = hash(((2*i+1, 2*j), seed))
    # myhash(midpoint((i, j), (i+1, j+1))) = hash(((2*i+1, 2*j+1), seed))
    # myhash(midpoint((i, j), (i, j+1))) = hash(((2*i, 2*j+1), seed))
    # avoids any possibility of floating point weirdness, I believe.

    # To make this fast, we use the hash in Gustavson. This generalises
    # to higher dimensions in a straightforward way.
    
    hash0 = hasher.hash(rcentre0)
    hash1 = hasher.hash(rcentre1)
    hash2 = hasher.hash(rcentre2)
    # We can figure the coords of the centres in the natural coord system with
    # just one multiplication.

    magicnumber = A2 * (ix + iy + 1)
    
    # If we use the centres of the bumps in our noise calculations,
    # we guarantee more points have 0 as their noise value, which
    # isn't ideal, and reinforces the underlying simplex grid.
    # So, we make the centres of the bumps alternately
    # the centre of one simplex or the other that the bump is nonzero
    # on.
    
    if largex:
        
        if hash0 < 16:
            centre0 = ( ix + magicnumber + twothirds, 
                       iy + magicnumber + onethird )
        else:
            centre0 = ( ix + magicnumber + A2 + 1 + onethird, 
                       iy + magicnumber + A2 + twothirds)
        
        if hash1 < 16:
            centre1 = ( ix + magicnumber - A2 + onethird, 
                       iy + magicnumber - A2 - onethird )
        else:
            centre1 = ( ix + magicnumber + twothirds, 
                       iy + magicnumber + onethird )
        
        if hash2 < 16:
            centre2 = ( ix + magicnumber + twothirds, 
                       iy + magicnumber + onethird )
        else:
            centre2 = ( ix + magicnumber + onethird, 
                       iy + magicnumber + twothirds )
        
    else:

        if hash0 < 16:
            centre0 = ( ix + magicnumber + onethird, 
                       iy + magicnumber + twothirds )
        else:
            centre0 = ( ix + magicnumber + A2 + twothirds, 
                       iy + magicnumber + A2 + 1 + onethird )
        
        if hash1 < 16:
            centre1 = ( ix + magicnumber - A2 - onethird, 
                       iy + magicnumber - A2 + onethird )
        else:
            centre1 = ( ix + magicnumber + onethird, 
                       iy + magicnumber + twothirds )
        
        if hash2 < 16:
            centre2 = ( ix + magicnumber + twothirds, 
                       iy + magicnumber + onethird )
        else:
            centre2 = ( ix + magicnumber + onethird, 
                       iy + magicnumber + twothirds )
    
    # Calculating and summing the noise in pretty much the same way that
    # simplex does.
        
    noise0 = dot2D( gradient2D[ hash0 % 16 ], (x - centre0[0], y - centre0[1])
                    )* smoothbump0
    noise1 = dot2D( gradient2D[ hash1 % 16 ], (x - centre1[0], y - centre1[1])
                    ) * smoothbump1
    noise2 = dot2D( gradient2D[ hash2 % 16 ], (x - centre2[0], y - centre2[1])
                    ) * smoothbump2
    
    return noise0 + noise1 + noise2




# What follows is BCC for 3D. A much more modular, less expository approach is 
# taken.

# Some basic utility functions, good for any number of dimensions.


def int_and_frac_parts(x):
    """
    returns the integer (int) and fractional parts (0<=float<=1) of x
    """

    if x >= 0:
        ix = int(x)
    else:
        ix = int(x) - 1
    
    return ix, x - ix
    

def rankem(coords):
    """
    takes a list of coords and returns three lists: ranks, hasrank and 
    rankedcoords.
    
    rank[i] = j iff coord[i] is greater than j other coords and hasrank[j] = i 
    and rankedcoords[j] = coord[i].
    """
    
    n = len(coords)    
    
    if n == 1:
        return [0], [0], coords
    else:
        try:
            thiscoord = coords.pop()
        except IndexError:
            return [], [], []
        except AttributeError:
            coords = list(coords)
            thiscoord = coords.pop()
        
        rank, hasrank, rankedcoords = rankem(coords)

        thisrank = 0
        
        for i in range(n-1):
            if coords[i] > thiscoord:
                rank[i] += 1
            else:
                thisrank += 1

        rank.append(thisrank)
        hasrank.insert(thisrank, n-1)
        rankedcoords.insert(thisrank, thiscoord)
        
        return rank, hasrank, rankedcoords


class Bumpshape(object):
    """
    stores a Gradient and a flag to determine which centre a bump uses,
    generated using the hash that is passed to __init__.
    
    *** needs dimensionfacts to be defined ***
    """
    def __init__(self, num):
        n = num % dimensionfacts.twicenumberofGradients
        ndiv, nmod = divmod(n, dimensionfacts.numberofGradients)
        self.centre = ndiv
        self.grad = dimensionfacts.gradients[nmod]

def getbumpshapes(ivector, rank, hasrank, hasher):
    """
    gets all the hashes of n*facecentres of the home simplex,
    and turns them into Bumpshapes.
    """
    
    n = len(ivector)
    hashcentre = [ n*ivector[i] + rank[i] for i in range(n) ]
    shapes = [ Bumpshape(hasher.hash(tuple(hashcentre))) ]
    
    for i in range(n):
        hashcentre[hasrank[i]] += 1
        shapes.append( Bumpshape(hasher.hash(tuple(hashcentre))) )
    
    return shapes


def quickdot(gradient, vector):
    """
    gradients in gradient'n'D have entries that are either -1, 0, or 1. We
    can take advantage of that to write a quicker dot product.
    """
    value = 0
    for i in range(len(gradient)):
        if gradient[i] == 1:
            value += vector[i]
        elif gradient[i] == -1:
            value -= vector[i]            
    return value
    
    
class DimensionFacts(object):
    """
    number of gradients, and the a list of the Gradients themselves.
    Its a "run once" part of the code, so it can be a little slow.
    """
    def __init__(self, n):

        self.dimension = n

        self.A = (n + 1.0 - sqrt(n+1))/(n* (n+1)) # G in Gustavson
        self.B = (sqrt(n+1) - 1.0)/n              # F in Gustavson

        zeroes = n//3 # integer division
        a = [(1, -1) for i in range(n - zeroes)]
        sanszerogradients = itertools.product( *a )

        a = [range(n) for i in range(zeroes)]
        zeroplaces = itertools.product( *a )
        zeroplaces = [ p for p in zeroplaces if \
            all(p[i] < p[i+1] for i in range(zeroes-1))]
        
        gradients = []

        for grad in sanszerogradients:
            for places in zeroplaces:
                newgrad = [grad[i] for i in range(n-zeroes)]
                for i in range(zeroes):
                    newgrad.insert(places[i], 0)
                gradients.append(newgrad)
            
        self.gradients = [ Gradient(g, self.A + 0.25) for g in gradients]
        self.numberofGradients = len(gradients)
        self.twicenumberofGradients = 2*len(gradients)
        

class Gradient(object):
    """ stores a gradient and the scaled sum of its coords"""
    def __init__(self, vector, scalingfactor):
        self.vector = vector
        self.scaledsum = scalingfactor * sum(vector)

#everything below depends on the dimension being 3.

dimensionfacts = DimensionFacts(3)

A3 = dimensionfacts.A
B3 = dimensionfacts.B

gradient3D = dimensionfacts.gradients

# A pretty blah hack to get n*0.25 for small n cheaply.

quarters = [0, 0.25, 0.5, 0.75, -0.75, -0.5, -0.25]


def rectilinear_int_and_frac_vectors3D(x, y, z):
    """
    Converts (x, y, z) to rectilinear coords, and breaks them into integer
    and fractional parts.
    """
    
    v = B3*(x+y+z)
    rx = x + v
    ry = y + v
    rz = z + v
    
    ix, fx = int_and_frac_parts(rx)
    iy, fy = int_and_frac_parts(ry)
    iz, fz = int_and_frac_parts(rz)

    return [ix, iy, iz] , [fx, fy, fz]

    
def makebumps3D(c):
    """
    from a list of three ranked coords, we output the smooth bump function
    values. This is a very ad hoc, strictly for speed, function.
    """           
    a0 = c[0]
    a1 = c[1] - c[0]
    a2 = c[2] - c[1]
    a3 = 1    - c[2]
    
    b0 = c[1]
    b1 = c[2] - c[0]
    # b2 = 1 - b0
    # b3 = 1 - b1
    
    a0a1 = a0 * a1
    a2a3 = a2 * a3
    a0a1a2 = a0a1 * a2
    a0a1a3 = a0a1 * a3
    a0a2a3 = a0 * a2a3
    a1a2a3 = a1 * a2a3
    
    b0b1 = b0 * b1
    b1b2 = b1 - b0b1         # = b1 * (1-b0) = b1 * b2
    b2b3 = 1 - b0 - b1 +b0b1 # = (1-b0)*(1-b1) = b2 * b3
    b3b0 = b0 - b0b1         # = (1-b1) * b0 = b3 * b0
    
    bump0 = a1a2a3 * b3b0
    bump1 = a0a2a3 * b0b1
    bump2 = a0a1a3 * b1b2
    bump3 = a0a1a2 * b2b3
    
    smoothbump0 = bump0 * bump0
    smoothbump1 = bump1 * bump1
    smoothbump2 = bump2 * bump2
    smoothbump3 = bump3 * bump3
    
    return [smoothbump0, smoothbump1, smoothbump2, smoothbump3] 


def BCCnoise3(x, y, z, hasher):
    """
    BCC noise in 3 dimensions. Should start to be a bit quicker with a
    promise of one fewer floating point multiplications than simplex.
    """
    
    ivector, fvector = rectilinear_int_and_frac_vectors3D(x, y, z)
    
    rank, hasrank, rankedcoords = rankem(fvector)

    smoothbump = makebumps3D(rankedcoords)
    
    bumpshape = getbumpshapes(ivector, rank, hasrank, hasher)

    # We can figure the coords of the centres in the natural coord system with
    # just one more multiplication.

    magicnumber = A3 * sum(ivector, 1.5)
    
    # Sometimes we want the randomisation centre to be the centre of the
    # simplex we're in, the home centre, or sometimes the centre of one of the
    # adjacent simplices.
    #
    # We decide this with bumpshape.centre. If its 0 we take the lesser of the
    # centres, if its 1 we take the greater, where the order of the centres is
    # determined lexicographically. The homecentre is always greater than 
    # the first alternative centre, and less than the last.
    #
    # The dot products that we calculate depend on which centre the bumpshape
    # decides, but it does so in a simple way. The difference between
    # dot products is always some number of quarters, and its not too hard to
    # work out how many.

    # homecentre is the centre of the simplex we're in.    
    
    # HomeCentre = (ivector[0] + magicnumber + quarters[rank[0] + 1],
    #               ivector[1] + magicnumber + quarters[rank[1] + 1],
    #               ivector[2] + magicnumber + quarters[rank[2] + 1])
    
    xyz_minus_home_centre = \
                 ( x - (ivector[0] + magicnumber + quarters[rank[0] + 1]),
                   y - (ivector[1] + magicnumber + quarters[rank[1] + 1]),
                   z - (ivector[2] + magicnumber + quarters[rank[2] + 1]) )
        
    gradient = bumpshape[0].grad
    noise0 = quickdot(gradient.vector, xyz_minus_home_centre)

    if bumpshape[0].centre == 0:
        noise0 -= gradient.scaledsum + quarters[gradient.vector[hasrank[0]]] 

    gradientvector = bumpshape[1].grad.vector
    noise1 = quickdot(gradientvector, xyz_minus_home_centre)
        
    if (bumpshape[1].centre == 0) != (hasrank[0] > hasrank[1]): # != is xor
        noise1 += quarters[ gradientvector[hasrank[1]] +
                            gradientvector[hasrank[0]] ]

    gradientvector = bumpshape[2].grad.vector
    noise2 = quickdot(gradientvector, xyz_minus_home_centre)
        
    if (bumpshape[2].centre == 0) != (hasrank[1] > hasrank[2]): # != is xor
        noise2 += quarters[ gradientvector[hasrank[2]] +
                            gradientvector[hasrank[1]] ]

    gradient = bumpshape[3].grad
    noise3 = quickdot(gradient.vector, xyz_minus_home_centre)
        
    if bumpshape[3].centre == 1:
        noise3 += gradient.scaledsum + quarters[gradient.vector[hasrank[2]]]


    return noise0 * smoothbump[0] + noise1 * smoothbump[1] \
           + noise2 * smoothbump[2] + noise3 * smoothbump[3]
