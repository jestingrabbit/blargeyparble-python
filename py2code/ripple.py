__author__ = 'bill'

""" creates a sort of ripplish pattern, for the top of the bucket."""


from math import sqrt, floor, cos, hypot, pi

import model
import random


def ripple(radius2 = 208, peaks = 4, peak_centred = True):

    r = sqrt(208)

    dx = int( 2*floor(r) + 7 )

    palette = [(255, 255, 255), # clear glass
               (192, 192, 192), # outside bucket
               (127, 127, 255), # light blue glass
               (  0,   0, 255), # dark blue glass
               (  0, 255,   0), # aqua glass
               (255, 255,   0)] # torch

    m = model.Model(dx=dx,
                    dy=dx,
                    flat=True,
                    name="ripple",
                    palette= palette)

    

    half_periods = 2*peaks

    if peak_centred:
        half_periods -= 1

    A = half_periods * pi / r

    if peak_centred:
        B = 0
    else:
        B = pi

    m.append_grid('b', "0")
    g = m.last_grid()

    for x, y in m:
        if x**2 + y**2 > 208:
            g[x, y] = 1

    random.seed()

    for x, y in m:
        if g[x, y] == 0:
            threshold  = 0.5 + 0.5 * cos( A*hypot(x, y) + B)

            var = random.random()

            if var < threshold - 0.5:
                g[x, y] = 3
            elif var < threshold:
                g[x, y] = 2

    random.seed()

    C = sqrt(2)*A
    D = 1.5 * A

    for x, y in m:
        if g[x, y] == 2:
            threshold  = 0.5 + 0.5 * cos( C*hypot(x + A, y + D) )

            var = random.random()

            if var < threshold - 0.6:
                g[x, y] = 4

    quantities = [0, 0, 0, 0, 0]


    for x, y in m:
        quantities[ g[x, y] ] += 1

    print quantities

    m.make_plan()

    
    
    

    


