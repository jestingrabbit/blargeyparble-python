"""
For minopticon: a spiral wall drop design.
"""

__author__ = 'bill'


import math
import model


def spiral_wall_design(radius2,
                       gradient=1,
                       blockcentred=True,
                       number_of_stripes=1,
                       width_of_stripes=0.5,
                       revolutions=1):

    r = math.sqrt(radius2)
    rint = int(r)
    pi = math.pi

    if blockcentred:
        dx = 2*rint + 5
    else:
        dx = 2*rint + 6

    dy = dx

    h = int(revolutions * 2 * pi * r / gradient)

    dz = h + 12
    cz = 2

    m = model.Model(dx=dx,
                    dy=dy,
                    dz=dz,
                    cz=cz,
                    name="spiral",
                    palette=[(255, 255, 255),
                             (170, 170, 0), (255, 255, 85),
                             (170, 0, 170), (255, 85, 255),
                             (255, 0, 0)])

    m.append_grid('b', "0")
    g = m.last_grid()

    for x, y, z in m:
        if z >= -1 and (x**2 + y**2 <= radius2):
            g[x, y, z] = 1

    m.append_hollowed_grid()
    g = m.last_grid()

    for x, y, z in m.i.iter_indices_z_level(-1):
        g[x, y, z] = 0

    threshold = width_of_stripes*2*pi/number_of_stripes

    for x, y, z in m:

        if g[x, y, z] != 0:

            angle = math.atan2(y, x)
            twist = 2*pi + z/(gradient*r)
            twistednormalisedangle = math.fmod(twist + angle, 2*pi/number_of_stripes)

            if twistednormalisedangle > threshold:
                g[x, y, z] = 3

    m.append_grid('b', "0")
    h = m.last_grid()

    for x, y, z in m:
        if z >= 0:
            if g[x, y, z] == g[x, y, z-1]:
                h[x, y, z] = g[x, y, z]
            else:
                h[x, y, z] = g[x, y, z] + 1

    m.add_torches()

    m.make_plan()