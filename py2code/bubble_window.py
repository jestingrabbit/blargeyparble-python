"""
created 2 may 2013

outputs a blueprint for a spherical cap to cover a hole with radius**2 == 'radius2' that is 'depth' blocks deep.
"""

import model
import math


def bubble_window(radius2, depth=3, blockcentred=True):
    """
     radius2 is the square of the radius of the hole you want the window over. depth is the how many blocks deep
     the bubble is ie how wide a ledge you can build inside the window. blockcentred iff the "diameter" of the hole
     is odd.
    """

    if blockcentred:

        dx = 2*int(math.sqrt(radius2)) + 5
        dz = dx
        dy = depth + 3

        xoffset = 0
        zoffset = 0

    else:
        dx = 2*int(math.sqrt(radius2)) + 6
        dz = dx
        dy = depth + 3

        xoffset = 0.5
        zoffset = 0.5

    m = model.Model(dx=dx,
                    dy=dy,
                    dz=dz,
                    cy=0,
                    name="bubble_window",
                    palette=[(255, 255, 255), (0, 0, 0), (85, 85, 85), (170, 170, 170), (255, 0, 0)])

    d = depth + 0.5
    r = (radius2 + d**2)/(2*d)
    r2 = r**2

#    print (r2 - (r - d)**2) # should be radius2

    def f(x, y, z):
        v = (x + xoffset)**2 + (y + r - d)**2 + (z + zoffset)**2
        if v <= r2:
            return 1
        else:
            return 0

    m.append_grid('b', f)
    m.append_hollowed_grid()

    g = m.last_grid()
    m.append_grid('b', "0")
    h = m.last_grid()

    for z0 in m.i.range_z()[1:]:
        for (x, y, z) in m.i.iter_indices_z_level(z0):
            if g[x, y, z] == 0 and g[x, y, z-1] == 1:
                # pale grey for "empty this level, full below"
                h[x, y, z] = 3
            elif g[x, y, z] == 1 and g[x, y, z-1] == 0:
                # dark grey for "full this level, empty below"
                h[x, y, z] = 2
            elif g[x, y, z] == 1 and g[x, y, z-1] == 1:
                # black for "put a block on a block"
                h[x, y, z] = 1

    m.add_torches()
    m.make_plan()