'''
Created on Apr 5, 2012

@author: bill
'''

from itertools import starmap,izip
from operator import mul
from math import cos, sin

def dot(vector1, vector2):
    return sum(starmap(mul,izip(vector1,vector2)))

def cross((a, b, c),
          (d, e, f)):
    return (b*f - c*e, c*d - a*f, a*e - b*d)

def rotation(theta):
    """ returns a function that rotates a 2-vector by theta.
    """

    costheta = cos(theta)
    sintheta = sin(theta)

    def f(x, y):
        return x*costheta - y*sintheta, x*sintheta + y*costheta

    return f



def squarezaxisrotations(x, y, z):
    return [(x, y, z), (-y, x, z), (-x, -y, z), (y, -x, z)]

def squarezaxisrotationsandreflections(x, y, z):
    return squarezaxisrotations(x, y, z) + squarezaxisrotations(-x, y, z)
