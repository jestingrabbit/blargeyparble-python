__author__ = 'bill'

"""mmmm shai-hulud."""

import math
import model
import random

from math import sqrt, pi
from vector import dot, cross, rotation

def shaihulud(worm_radius = 5,
              arc_radius = 20,
              angle_of_rotation = pi/4,
              angle_to_the_sand = pi/3,
              angle_out_of_sand = 2*pi/3,
              banding_gap_vs_diameter = 0.5,
              mouth_opening_vs_diameter = 1.5,
              head_length = 12):

    dx = 2*(arc_radius + worm_radius) + 9
    dy = dx
    dz = arc_radius + worm_radius + 9
    cz = 4

    m = model.Model(dx=dx,
                    dy=dy,
                    dz=dz,
                    cz=cz,
                    name="Great_Maker")

    if angle_of_rotation == pi/4 and angle_to_the_sand == pi/3:

        sqrtahalf = sqrt(0.5)
        sqrtthree = sqrt(3)

        def coord_rotation(x, y, z):

            x, y = sqrtahalf*x, sqrtahalf*y
            x, y = x+y, -x+y
            y, z = 0.5*(-y + sqrtthree*z), -0.5*(sqrtthree*y + z)

            return x, y, z

    else:

        cos_rotation = math.cos(angle_of_rotation)
        sin_rotation = math.sin(angle_of_rotation)

        cos_to_the_sand = math.cos(pi - angle_to_the_sand)
        sin_to_the_sand = math.sin(pi - angle_to_the_sand)

        def coord_rotation(x, y, z):

            x, y = cos_rotation*x + sin_rotation*y, -sin_rotation*x + cos_rotation*y
            y, z = cos_to_the_sand*y + sin_to_the_sand*z, -sin_to_the_sand*y + cos_to_the_sand*z

            return x, y, z

    wr2 = worm_radius**2

    def inside_torus(x, y, z):

        return (arc_radius - sqrt(x**2 + y**2))**2 + z**2 < wr2

    if angle_out_of_sand >= pi:

        has_mouth = False

        def inside_the_worm_body(x, y, z):
            return inside_torus(x, y, z)

    else:

        has_mouth = True

        theta = pi/2 - angle_out_of_sand
        phi = pi - angle_out_of_sand

        head_direction = (         math.cos(theta),          math.sin(theta), 0)
        perp_direction = (           math.cos(phi),            math.sin(phi), 0)
        head_position  = (arc_radius*math.cos(phi), arc_radius*math.sin(phi), 0)

        lwr  = worm_radius * mouth_opening_vs_diameter
        a = (lwr- worm_radius)/head_length**2

        def inside_the_worm_body(x, y, z):

            head_component = dot((x, y, z), head_direction)
            perp_component = dot((x, y, z), perp_direction) - arc_radius

            if head_component <= 0:
                return inside_torus(x, y, z)

            if z>lwr or -z>lwr or perp_component>lwr or -perp_component>lwr:
                return False

            if head_component >= head_length:
                return False

            radius = sqrt(perp_component**2 + z**2)

            return radius < worm_radius + head_component**2 * a


    m.append_grid('b', "0")
    g = m.last_grid()

    for x, y, z in m:

        xr, yr, zr = coord_rotation(x, y, z)

        if inside_the_worm_body(xr, yr, zr):
            g[x, y, z] = 1

    

    m.append_grid('b', "0")
    h = m.last_grid()

    for x, y, z in m:

        if g[x, y, z] == 1:
            if g[x, y, z+1] == 0:
                xr, yr, zr = coord_rotation(x, y, z + 0.5)

                if inside_the_worm_body(xr, yr, zr):
                    h[x, y, z+1] = 2
                    h[x, y, z] = 1
                else:
                    h[x, y, z]   = 2
            else:
                h[x, y, z] = 1

    g = m.last_grid()
    m.append_grid('b', "0")
    h = m.last_grid()

    for x, y, z in m:

        if g[x, y, z] == 1:

            visible_block = any( [ (v % 2) == 0  for v in g.neighbour_values(x, y, z) ] )
    
            if not visible_block:
                h[x, y, z] = 7

        else:
            h[x, y, z] = g[x, y, z]


    g = m.last_grid()

    if has_mouth:

        random.seed()

        random_angle = random.uniform(0, 2*pi/3)

        rot_random = rotation(random_angle)

        rot_2pion3 = rotation(2*pi/3)

        v = ( head_length, worm_radius*(1 + mouth_opening_vs_diameter), 0)

        y, z = v[1], v[2]
        y, z = rot_random(y, z)

        v0 = (v[0], y, z)

        y, z = rot_2pion3(y, z)

        v1 = (v0[0], y, z)

        y, z = rot_2pion3(y, z)

        v2 = (v0[0], y, z)

        n0 = cross(v0, v1)
        n1 = cross(v1, v2)
        n2 = cross(v2, v0)

        n = [n0, n1, n2]

        c = dot( n0, (head_length/(1+ mouth_opening_vs_diameter), 0, 0))

        def inside_the_worm_mouth(x, y, z):

            behindplanes = 0

            for i in range(3):
                if (0 <= c) == (dot(n[i], (x, y, z)) <= c):
                    behindplanes += 1

            return behindplanes >= 2

    for x, y, z in m:

        xr, yr, zr = coord_rotation(x, y, z)

        hc, pc, zr = dot(head_direction, (xr, yr, zr)), dot(perp_direction, (xr, yr, zr)) - arc_radius, zr

        if g[x, y, z] != 0 and hc>0:
            if not inside_the_worm_mouth(hc, pc, zr):
                g[x, y, z] = 0



    g = m.last_grid()
    m.append_grid('b', "0")
    h = m.last_grid()

    for x, y, z in m:

        if g[x, y, z] == 7:

            visible_block = any( [ (v % 2) == 0  for v in g.neighbour_values(x, y, z)
    
            if visible_block:
                h[x, y, z] = 7
            else:
                h[x, y, z] = 0

        else:
            h[x, y, z] = g[x, y, z]












    print len(m.grids)

    m.set_palette([(255, 255, 255), (0, 0, 0),(85, 85, 85),(0, 0, 0),(0, 0, 0),(0, 0, 0),(0, 0, 0), (255, 0, 0), (0, 255, 0), (0, 0, 255)])

    m.make_plan()

    print("here")
