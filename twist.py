__author__ = 'bill'

"""thrice bound."""

import math
import model
import rainbow


from math import sqrt, pi, fabs, sin, cos, atan2, floor, fmod
from vector import dot, cross, rotation
from interpolate import linear2D

pion2 = pi * 0.5
twopi = 2*pi
thirdtwopi = pi * (2/3)
tol = (0.5)**40


def nr_angle_calc(x, y, est, G2 = 1, tolerance = tol):
    """assumes greatradius == 1 ie, the curve is of
    the form (cos t, sin t, gt), that est is a goodish 
    approximation and finds the t such that
    d((x, y, 0), (cos t, sin t, gt)) is minimised,
    and the distance squared."""
    
    diff = 1
    
    lower = 0
    lowerf = -y
    theta = atan2(y, x)
    upper = theta
    upperf = G2 * theta
    
    while not -tolerance < diff < tolerance:            
        sint = sin(est)
        cost = sqrt( 1- sint**2 )
        if est > pion2:
            cost = - cost

        ft = G2*est + x * sint - y * cost
        
        if ft == 0:
            return est, (x - cost)**2 + (y - sint)**2 + G2* (est ** 2)
        
        fprimet = G2 + x * cost + y * sint
        
        if fprimet != 0:
            possiblediff = ft/fprimet
            possibleest = est - possiblediff
        else:
            possibleest = lower-1
        
        if lower < possibleest < upper:
            est = possibleest
            diff = possiblediff
            
            if ft<0:
                lower = est
                lowerf = ft
            else:
                upper = est
                upperf = ft
                
        else:
            d1 = (upperf-ft)
            d2 = (lowerf-ft)
            d3 = (lowerf-upperf)
            d = d1*d2*d3
            
            if d != 0:
                s1 = d1*upperf*ft*lower
                s2 = d2*lowerf*ft*upper
                s3 = d3*upperf*lowerf*est
                another = (s1 - s2 + s3)/d

            if ft<0:
                lower = est
                lowerf = ft
            else:
                upper = est
                upperf = ft

            if d!=0:
                if lower < another < upper:
                    est = another
                else:
                    est = 0.5*(upper + lower)
            else:
                est = 0.5*(upper + lower)
                
            diff = upper - lower

    return est, (x - cos(est))**2 + (y - sin(est))**2 + G2* (est ** 2)



def angles_and_quadratures(gradient = 1,
                           greatradius = 20,
                           resolution = 8,
                           tuberadius = None,
                           display = False):
    
    if tuberadius == None:
        dx = 2 * int( (greatradius*2 + 2) * resolution) + 5
    else:
        dx = 2 * int( (greatradius + tuberadius + 2) * 32) + 5
    
    dy = dx

    m = model.Model(dx=dx,
                    dy=dy,
                    flat=True,
                    name="angles_and_quads",
                    palette= rainbow.rb)
                    
                    
    m.append_grid('f')
    angles = m.last_grid()

    m.append_grid('f')
    quadratures = m.last_grid()

    normaliser = 1/(greatradius*resolution)
    renormaliser2 = greatradius ** 2
    
    G2 = gradient ** 2
    G4 = G2 ** 2

    bound = int((dx -1)//2)

    for x in range(bound):
        xnew = x/resolution
        quadratures[x, 0] = (xnew- greatradius)**2

    x = bound
    xnew = x * normaliser
    for y in range (1, bound+1):
        ynew = y * normaliser
        angle, quadrature = nr_angle_calc( xnew, ynew, angles[x, y-1], G2 ) 
        angles[x, y] = angle
        quadratures[x, y] = quadrature * renormaliser2

    m.append_grid('i')
    nearest = m.last_grid()
    
    """ we'll nearest to give an accuratish prediction of where to find
    a good estimate of the next angle to calculate, but its meaning will
    vary from block to block. Here, nearest[x, y] = 0 iff angles[x+1, y] is
    good, and nearest[x, y] = 1 iff angles[x, y-1] is good."""
    
    for pseudx in range (1, bound+1):
        x = bound - pseudx
        xnew = x * normaliser

        for y in range (1, bound+1):
            ynew = y * normaliser
        
            if nearest[x+1, y] + nearest[x, y-1] < 2:
                est = angles[x+1, y]
            else:
                est = angles[x, y-1]
            
            angle, quadrature = nr_angle_calc( xnew, ynew, angles[x, y-1], G2 ) 
            angles[x, y] = angle
            quadratures[x, y] = quadrature * renormaliser2

            if fabs(angle - angles[x+1, y]) < fabs(angle - nearest[x, y-1]):
                nearest[x, y] = 0
            else:
                nearest[x, y] = 1

    """and now, nearest[x, y] = 0 iff angles[x+1, y] is good, and 
    nearest[x, y] = 1 iff angles[x, y+1] is good."""

    y = bound
    ynew = y * normaliser
    for pseudx in range (1, bound+1):
        x = -pseudx
        xnew = x * normaliser
        angle, quadrature = nr_angle_calc( xnew, ynew, angles[x+1, y], G2 ) 
        angles[x, y] = angle
        quadratures[x, y] = quadrature * renormaliser2
        nearest[x, y] = 1
    
    for pseudy in range (1, bound+1):
        y = bound - pseudy
        ynew = y * normaliser

        for pseudx in range (1, bound+1):
            x = -pseudx
            xnew = x * normaliser
        
            if nearest[x+1, y] + nearest[x, y+1] < 1:
                est = angles[x+1, y]
            else:
                est = angles[x, y+1]
            
            angle, quadrature = nr_angle_calc( xnew, ynew, est, G2 ) 
            angles[x, y] = angle
            quadratures[x, y] = quadrature * renormaliser2

            if fabs(angle - angles[x+1, y]) < fabs(angle - nearest[x, y+1]):
                nearest[x, y] = 0
            else:
                nearest[x, y] = 1
    
    for y in range(1, bound+1):        
        for x in range(-bound, bound+1):
            angles[x, -y] = -angles[x, y]
            quadratures[x, -y] = quadratures[x, y]
            nearest[x, -y] = nearest[x, y]
    
    if display:            
        m.append_grid('i')
        iangles = m.last_grid()
    
        m.append_grid('i')
        iquadratures = m.last_grid()
        
        for x, y in m:        
            iangles[x, y] = int( angles[x, y]*256 ) % 256
            iquadratures[x, y] = int( quadratures[x, y]) % 256
    
        m.make_plan(-2, "angles")
        m.make_plan(-1, "quadratures")
        
    return angles, quadratures
    

def helix(gradient = 1,
          greatradius = 20,
          tuberadius = 10,
          height = 200,
          angularoffset = 0):
              
    """ a tower wrapped around a curve """
    
    heightgrowthconstant = greatradius * gradient
    R2 = (greatradius + tuberadius)**2
    tr2 = tuberadius**2
    
    def curve(t):
        """the curve the helix is around."""
        
        return (greatradius * cos(t + angularoffset), 
                greatradius * sin(t + angularoffset),
                heightgrowthconstant * t)
    
    resolution = 16
    
    a, q = angles_and_quadratures(gradient,
                                  greatradius,
                                  resolution = resolution,
                                  tuberadius = 10,
                                  display = False)
    
    dx = 2*int(greatradius + tuberadius) + 5
    dy = dx
    dz = int(height + 2*tuberadius + 5)
    
    cz = int(tuberadius + 3)
    
    m = model.Model(dx=dx,
                    dy=dy,
                    dz=dz,
                    cz=cz,
                    name="helix")
    
    m.append_grid('b')
    g = m.last_grid()
    
    for z in m.i.range_z():
        leveloffset = z/heightgrowthconstant + angularoffset
        leveloffsetm = fmod(leveloffset, twopi)
        
        if leveloffsetm > pi:
            leveloffsetm = leveloffsetm - twopi
        
        rot = rotation(leveloffsetm)
        
        for x, y, z in m.i.iter_indices_z_level(z):
            if x**2 + y**2 < R2:            
                newx, newy = rot(x, y)
                newx *= resolution
                newy *= resolution
                ang = linear2D(newx, newy, a)
                
                if 0 <= leveloffset + ang <= height/heightgrowthconstant:
                    
                    quad = linear2D(newx, newy, q)
                    
                    if quad > tr2:
                        g[x, y, z] = 0
                    else:
                        g[x, y, z] = 1
                else:
                    g[x,y,z] = 0            
            else:
                g[x, y, z] = 0

    m.append_hollowed_grid()
    g = m.last_grid()
    m.append_grid('b')
    h = m.last_grid()
    
    for x, y, z in m:
        if g[x, y, z] != 0:
            h[x, y, z] += 2
            if z+1 <= m.i.max_z():
                h[x, y, z+1] += 1
    
    m.set_palette([(255,255,255),  (170,170,170), (85,85,85), (0,0,0)])
    m.make_plan()
    
            
    
            
def treefarm(greatradius = 26,
             height = 200):
    
    gradient = sqrt(0.5)
    heightgrowthconstant = greatradius * gradient
    invheightgrowthconstant = 1/heightgrowthconstant
    tuberadius = greatradius - sqrt(11)
    R2 = (greatradius + tuberadius)**2
    tr2 = tuberadius**2
    
    def curve(t):
        """a curve a helix is around. the others are 2pi/3 rotations of it."""
        
        return (greatradius * cos(t), 
                greatradius * sin(t),
                heightgrowthconstant * t)
    
    resolution = 32
    
    a, q = angles_and_quadratures(gradient,
                                  greatradius,
                                  resolution = resolution,
                                  display = False)
    
    dx = 4*int(greatradius) + 7
    dy = dx
    dz = int(height + 2*greatradius + 7)
    
    cz = int(greatradius + 4)
    
    m = model.Model(dx=dx,
                    dy=dy,
                    dz=dz,
                    cz=cz,
                    name="treefarm")
    
    m.append_grid('b')
    wood0 = m.last_grid()
    m.append_grid('b')
    wood1 = m.last_grid()
    m.append_grid('b')
    wood2 = m.last_grid()
    
    mark1 = 0
    
    m.append_grid('b')
    flag0 = m.last_grid()
    m.append_grid('b')
    flag1 = m.last_grid()
    m.append_grid('b')
    flag2 = m.last_grid()
    
    mark2 = 3
    
    tmin = 0
    tmax = height*invheightgrowthconstant
    
    p = [(greatradius, 0, 0),
         (-greatradius*0.5, -sqrt(3)*0.5*greatradius, 0),
         (-greatradius*0.5, +sqrt(3)*0.5*greatradius, 0)]
    
    for z in m.i.range_z():
        tvaluelevel = z/heightgrowthconstant
        tvaluelevelm = fmod(tvaluelevel, twopi)
        
        if tvaluelevelm > pi:
            tvaluelevelm = tvaluelevelm - twopi
        
        rot = [ rotation(tvaluelevelm),
                rotation(tvaluelevelm + twopi/3),
                rotation(tvaluelevelm - twopi/3) ]
        
        for x, y, z in m.i.iter_indices_z_level(z):
            for i in range(3):
                if x**2 + y**2 < R2:            
                    newx, newy = rot[i](x, y)
                    newx *= resolution
                    newy *= resolution
                    ang = linear2D(newx, newy, a)
                    
                    if ang > 0:
                        (m.grids[i+3])[x, y, z] = 1
                    
                    if 0 <= tvaluelevel - ang <= height + 1:
                        quad = linear2D(newx, newy, q)
                        
                        if quad < tr2:
                            (m.grids[i])[x, y, z] = 1
                        
                        if ang + tvaluelevel >= height:
                            (m.grids[i+3])[x, y, z] = 1
                        
                    elif tvaluelevel - ang < 0:
                        
                        quad = (p[i][0] - x) ** 2 + (p[i][1] - y) ** 2 \
                                +(p[i][2] - z) ** 2
                        
                        if quad < tr2:
                            (m.grids[i])[x, y, z] = 1
                        
                        if z > 0:
                            (m.grids[i+3])[x, y, z] = 1
    
    for i in range(3):
        m.append_hollowed_grid(entry = i)
    
    mark3 = 6

    for x, y, z in m:
        for i in range(3):
            if (m.grids[mark3 + i][x, y, z] != 0) and m.grids[mark2 + i][x, y, z] != 0:
                m.grids[mark3 + i][x, y, z] = 0
    
    m.append_grid('b', "0")
    g = m.last_grid()
    
    for x, y, z in m:
        for i in range(3):
            if m.grids[mark3 + i][x, y, z] != 0:
                g[x, y, z] = i+1
    
    m.append_grid('b', "0")
    h = m.last_grid()

    for x, y, z in m:
        if g[x, y, z] != 0:
            h[x, y, z] = g[x, y, z]
            if z > m.i.min_z() and g[x, y, z-1] !=0:
                h[x, y, z] += 4
        elif z > m.i.min_z() and g[x, y, z-1] !=0:
                h[x, y, z] = 4
                
                
    m.set_palette([(255,255,255),(255,0,0),(0,255,0),(0, 0, 255),\
     (170, 170, 170), (128,0,0),(0,128,0),(0, 0, 128)])
     
    m.palette.append((255, 255, 0))
    m.add_torches()
    m.palette.append((255, 170, 0))
    m.add_centres()
    m.make_plan()  


def vartreefarm(greatradius = 26,
             height = 200):
    
    gradient = sqrt(0.5)
    heightgrowthconstant = greatradius * gradient
    invheightgrowthconstant = 1/heightgrowthconstant
    tuberadius = greatradius - sqrt(11)
    R2 = (greatradius + tuberadius)**2
    tr2 = tuberadius**2
    
    def curve(t):
        """a curve a helix is around. the others are 2pi/3 rotations of it."""
        
        return (greatradius * cos(t), 
                greatradius * sin(t),
                heightgrowthconstant * t)
    
    resolution = 32
    
    a, q = angles_and_quadratures(gradient,
                                  greatradius,
                                  resolution = resolution,
                                  display = False)
    
    dx = 4*int(greatradius) + 7
    dy = dx
    dz = int(height + 2*greatradius + 7)
    
    cz = int(greatradius + 4)
    
    m = model.Model(dx=dx,
                    dy=dy,
                    dz=dz,
                    cz=cz,
                    name="vartreefarm")
    
    m.append_grid('b')
    wood0 = m.last_grid()
    m.append_grid('b')
    wood1 = m.last_grid()
    m.append_grid('b')
    wood2 = m.last_grid()
    
    mark1 = 0
    
    m.append_grid('b')
    flag0 = m.last_grid()
    m.append_grid('b')
    flag1 = m.last_grid()
    m.append_grid('b')
    flag2 = m.last_grid()
    
    mark2 = 3
    
    tmin = 0
    tmax = height*invheightgrowthconstant
    
    p = [(greatradius, 0, 0),
         (-greatradius*0.5, -sqrt(3)*0.5*greatradius, 0),
         (-greatradius*0.5, +sqrt(3)*0.5*greatradius, 0)]
    
    for z in m.i.range_z():
        tvaluelevel = z/heightgrowthconstant
        tvaluelevelm = fmod(tvaluelevel, twopi)
        
        if tvaluelevelm > pi:
            tvaluelevelm = tvaluelevelm - twopi
        
        rot = [ rotation(tvaluelevelm),
                rotation(tvaluelevelm + twopi/3),
                rotation(tvaluelevelm - twopi/3) ]
        
        for x, y, z in m.i.iter_indices_z_level(z):
            for i in range(3):
                if x**2 + y**2 < R2:            
                    newx, newy = rot[i](x, y)
                    newx *= resolution
                    newy *= resolution
                    ang = linear2D(newx, newy, a)
                    
                    if ang > 0:
                        (m.grids[i+3])[x, y, z] = 1
                    
                    if 0 <= tvaluelevel - ang <= height:
                        quad = linear2D(newx, newy, q)
                        
                        if quad < tr2:
                            (m.grids[i])[x, y, z] = 1
                        
                        if tvaluelevel-ang >= twopi:
                            (m.grids[i+3])[x, y, z] = 1
                        
                    elif tvaluelevel - ang < 0:
                        
                        quad = (p[i][0] - x) ** 2 + (p[i][1] - y) ** 2 \
                                +(p[i][2] - z) ** 2
                        
                        if quad < tr2:
                            (m.grids[i])[x, y, z] = 1
                        
                        if z > 0:
                            (m.grids[i+3])[x, y, z] = 1

    
    for i in range(3):
        m.append_hollowed_grid(entry = i)
    
    mark3 = 6

    for x, y, z in m:
        for i in range(3):
            if (m.grids[mark3 + i][x, y, z] != 0) and m.grids[mark2 + i][x, y, z] != 0:
                m.grids[mark3 + i][x, y, z] = 0
    
    m.append_grid('b', "0")
    g = m.last_grid()
    
    for x, y, z in m:
        for i in range(3):
            if m.grids[mark3 + i][x, y, z] != 0:
                g[x, y, z] = i+1
    
    m.append_grid('b', "0")
    h = m.last_grid()

    for x, y, z in m:
        if g[x, y, z] != 0:
            h[x, y, z] = g[x, y, z]
            if z > m.i.min_z() and g[x, y, z-1] !=0:
                h[x, y, z] += 4
        elif z > m.i.min_z() and g[x, y, z-1] !=0:
                h[x, y, z] = 4
                
                
    m.set_palette([(255,255,255),(255,0,0),(0,255,0),(0, 0, 255),\
     (170, 170, 170), (128,0,0),(0,128,0),(0, 0, 128)])
     
    m.palette.append((255, 255, 0))
    m.add_torches()
    m.palette.append((255, 170, 0))
    m.add_centres()
    m.make_plan()      