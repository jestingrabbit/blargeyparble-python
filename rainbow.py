redtogreen = [(255-i, i, 0) for i in range(255)]
greentoblue = [(0, 255-i, i) for i in range(255)]
bluetored = [(i, 0, 255-i) for i in range(255)]

fullrainbow = redtogreen + greentoblue + bluetored

f = 3*255/256

rb = [fullrainbow[ int(i*f) ] for i in range(256)]