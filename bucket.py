"""
A big bucket, made with half blocks.
"""

__author__ = 'bill'

import model
import math


def bucket(lower_radius=11, use_half_blocks=True):
    """

    :param lower_radius:
    :param use_half_blocks:
    :return:
    """
    r = int(lower_radius) + 0.5

    C = lower_radius / 6.5
    R = int(9.0 * C) + 0.5

    if use_half_blocks:
        height = 2 * int(14.0 * C)
    else:
        height = int(14.0 * C)

    H = - height * r / (R - r)
    alpha = r / H
    alpha2 = alpha ** 2

    def inside(x, y, z):
        return x ** 2 + y ** 2 <= alpha2 * (z - H) ** 2 and 0 <= z <= height

    dx = int(2 * R + 5)
    dy = dx
    dz = height + 5

    m = model.Model(dx=dx,
                    dy=dy,
                    dz=dz,
                    cz=2,
                    name="bucket",
                    palette=[(255, 255, 255), (0, 0, 0), (170, 170, 170),
                             (0, 255, 0), (0, 0, 255), (255, 0, 0)])

    m.append_grid('b', inside)
    m.append_hollowed_grid()
    g = m.last_grid()
    m.append_grid('b', "0")
    h = m.last_grid()

    for x, y, z in m:

        if z == height:

            if g[x, y, z] == 1 and g[x + 1, y, z] + g[x - 1, y, z] + g[x, y + 1, z] + g[x, y - 1, z] < 4:
                h[x, y, z] = 1
            else:
                h[x, y, z] = 0

        else:
            h[x, y, z] = g[x, y, z]

    if use_half_blocks:

        g = m.last_grid()
        m.append_grid('b', "0")
        h = m.last_grid()

        for x, y, z in m:
            if z % 2 == 0 and z + 1 < m.i.max_z():
                if g[x, y, z] + g[x, y, z + 1] == 0:
                    h[x, y, z / 2] = 0
                elif g[x, y, z] + g[x, y, z + 1] == 2:
                    h[x, y, z / 2] = 1
                elif g[x, y, z] == 1:
                    h[x, y, z / 2] = 3
                elif g[x, y, z + 1] == 1:
                    h[x, y, z / 2] = 4

    g = m.last_grid()
    m.append_grid('b', "0")
    h = m.last_grid()

    for x, y, z in m:
        if z > m.i.min_z():
            if g[x, y, z] == 0 and g[x, y, z - 1] != 0:
                h[x, y, z] = 2
            else:
                h[x, y, z] = g[x, y, z]

    m.add_torches()

    if use_half_blocks:
        m.make_plan(zrange=list(range(m.i.min_z(), dz / 2 + 5)))

    else:
        m.make_plan()


def bucket_made_of(blocks=128):
    r = 11.5
    C = 11 / 6.5
    R = int(9.0 * C) + 0.5
    h = int(14.0 * C)

    base_radius_to_height_ratio = h / r
    ratio = base_radius_to_height_ratio

    alpha = (R - r) / h
    alpha2 = alpha ** 2

    def inside(x, y, z):
        return x ** 2 + y ** 2 <= alpha2 * z ** 2

    def rtoz(radius):
        return int(radius / alpha)

    def ztoh(z):
        return int(z * alpha * ratio)

    maxradius = int(math.sqrt(blocks / math.pi))

    dx = maxradius + 5
    dy = dx
    cz = 0
    dz = rtoz(maxradius) + 5

    m = model.Model(dx=dx,
                    dy=dy,
                    dz=dz,
                    cz=cz,
                    name="specificbucket",
                    palette=[(255, 255, 255), (0, 0, 0), (170, 170, 170), (255, 0, 0)])

    m.append_grid('b', inside)
    g1 = m.last_grid()
    m.append_grid('b', "0")
    g2 = m.last_grid()

    plates = dz * [0]
    rims = dz * [0]

    for x, y, z in m:

        if g1[x, y, z] == 1:
            plates[z] += 1

            if g1.visible(x, y, z):
                rims[z] += 1
                g2[x, y, z] = 1

    guessradius = math.sqrt(blocks / (math.pi + ratio * 4 * math.sqrt(2)))

    z0 = rtoz(guessradius)

    def ztosa(z):

        return plates[z] + sum(rims[z + 1: z + ztoh(z) + 1])

    if ztosa(z0) <= blocks:

        while ztosa(z0) < blocks:
            z0 += 1

    elif ztosa(z0) > blocks:

        while ztosa(z0) > blocks:
            z0 -= 1

        z0 += 1

    print(ztosa(z0))

    m.make_plan( zrange = list(range(z0, z0 + ztoh(z0) + 2)))