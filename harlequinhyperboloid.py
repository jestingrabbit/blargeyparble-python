'''
Created on Apr 5, 2012

@author: bill
'''

import model, math, vector, itertools

def mysign(x):
    if x< 0:
        return 0
    else:
        return 1

def harlequinhyperboloid( smallradius = 8, high = False, low = False ):

    """makes a Model that is a particoloured hyperboloid of revolution."""

    d = 3 * smallradius + 1
    m = model.Model(dx=d+2, dy=d+2, dz=d, name = "harlequinhyperboloid")

    m.append_grid('i', "9*(x**2 + y**2) - 4*z**2")
    datagrid = m.grids[0]

# inner = points inside the hyperboloid

    inner = [(8, 0, 0)]

    for i in range(4):
        inner.append( (8, 2*i + 1, 3*i + 1) )
        inner.append( (8, 2*i + 1, 3*i + 2) )
        inner.append( (8, 2*i + 2, 3*i + 3) )
        
    inner.append((6,6,0))
    inner.append((11,0,12))

    maxinnervalue = max([datagrid.get(*v) for v in inner])
    minoutervalue = min([max(datagrid.neighbour_values(*v)) for v in inner])

# a runtime sanity check

    if minoutervalue < maxinnervalue:
        print("crap: minoutervalue = ", minoutervalue, " < ", "maxinnervalue = ", maxinnervalue)
        boundary = maxinnervalue
    else:
        print("maxinnervalue = ", maxinnervalue, "<", "minoutervalue = ", minoutervalue)
        if high:
            boundary = minoutervalue
        elif low:
            boundary = + maxinnervalue
        else:
            boundary = (minoutervalue + maxinnervalue)/2

    m.append_conditional_blocks_grid( "g<"+str(boundary))
    m.append_hollowed_grid()

    g = m.grids[-1]

#n1 and n2 are normals to some planes. ln1 and ln2 are lists of normals

    n1 = (0, 3, 2)
    n2 = (3, 3, 2*math.sqrt(2))
    ln1 = vector.squarezaxisrotations(*n1)
    ln2 = vector.squarezaxisrotations(*n2)


    def f(x, y, z):
        """returns abs of the dot of (x, y, z) with n1 and n2, and the number of positive dots with the
        square rotations of n1 and n2 ie how many planes you're on the positive side of"""

        p1 = [ vector.dot( (x, y, z) , v ) for v in ln1 ]
        p2 = [ vector.dot( (x, y, z) , v ) for v in ln2 ]

        return abs(p1[0]), abs(p2[0]), sum(map(mysign, p1 + p2))

#boundary blocks are on the boundary between differently coloured regions

    boundaryblocks1 = []
    boundaryblocks2 = []

    for z in m.i.range_z():

        min_n1_dot_and_vector = (10, (0,0,0))
        min_n2_dot_and_vector = (10, (0,0,0))

        for x, y, z in m.i.iter_indices_z_level(z):

            if g.get(x, y, z) == 1:

                abs_n1_dot, abs_n2_dot, value = f(x, y, z)

                if abs_n1_dot < min_n1_dot_and_vector[0]:
                    min_n1_dot_and_vector = (abs_n1_dot, (x, y, z))

                if abs_n2_dot < min_n2_dot_and_vector[0]:
                    min_n2_dot_and_vector = (abs_n2_dot, (x, y, z))

#this is where the particolouring happens (the %2 bit that is)

                g.put((value % 2) + 1, x, y, z)

        boundaryblocks1.append( min_n1_dot_and_vector[1] )
        boundaryblocks2.append( min_n2_dot_and_vector[1] )

#we want the lowest and highest half diamonds in the hyperboloid to be red,
#and the lowest and highest levels to be all obsidian = '3' and glowstone = '4'.

    count_of_1s = 0
    count_of_2s = 0

    for x, y, z in itertools.chain(m.i.iter_indices_z_level(-(m.i.cz)), m.i.iter_indices_z_level(m.i.cz)):

        value = g.get(x, y, z)

        if value != 0:

            if value == 1:
                count_of_1s += 1
            elif value == 2:
                count_of_2s += 1

            if x==0 or y==0 or abs(x) == abs(y):
                g.put(4, x, y, z)
            else:
                g.put(3, x, y, z)

    red_is_1 = count_of_1s > count_of_2s

    if count_of_1s == count_of_2s:

        print('ones and twos in the first and last level are equal?? does not compute!!')
    

#gotta fill in the rest of the boundary blocks, then sanity check for reflection through the xy plane
#ie if (x, y, z) is a boundary block, then so should (x, y, -z).

    full_obsidian_boundary = set([])
    full_glowstone_boundary = set([])

    boundaryblocks = list(zip(boundaryblocks1[1: -1], boundaryblocks2[1: -1]))

#I have a bunch of straight lines on the outside of the hyperboloid, the boundaries of regions differently
#coloured. They should intersect 40 times, or 24 times if you just count the ones that aren't already
#glowstoned. 8 of those 24 are guaranteed, but the other 16 take some luck. Am I lucky?

    lucky_levels = []
    unlucky = False
    magic = smallradius*(math.sqrt(2) - 1)


    for (b1, b2) in boundaryblocks:

        A1 = set(vector.squarezaxisrotationsandreflections(*b1))
        A2 = set(vector.squarezaxisrotationsandreflections(*b2))

        if len(A1) == 4:
            if len(A2) != 4:
                print("len(A1) == 4, len(A2) != 4... this is bad, really bad.")
            elif b1[2] != 0:
                print("len(A1) == 4, but z level != 0, what fresh hell is this!!")
            else:
                full_glowstone_boundary = full_glowstone_boundary | A1 | A2

        elif A1.issubset(A2):
            lucky_levels.append((b1[2], A2))

        else:

            full_obsidian_boundary = full_obsidian_boundary | A1 | A2

    l = len(lucky_levels)

    if l == 0:

        print("l==0. We're shit out of luck... eyeball it??")
        unlucky = True
        

    elif l%2 == 1:

        print("an odd number of lucky levels... crap.")
        unlucky = True

    elif l == 2:

        print("l==2, hooray!!")

        for (a, A) in lucky_levels:

            full_glowstone_boundary = full_glowstone_boundary | A

    else:

        lucky_z_values = [ a for (a, A) in lucky_levels ]

        reflection_invariant = all([ (-z in lucky_z_values) for z in lucky_z_values ])

        if reflection_invariant:

            blah = min( [ (abs(a - magic), a) for a in lucky_z_values])

            for (a, A) in lucky_levels:

                if abs(a) == abs(blah[1]):
                    full_glowstone_boundary = full_glowstone_boundary | A
                else:
                    full_obsidian_boundary = full_obsidian_boundary | A

        else:
            print("lucky_levels not reflection_invariant... really? really??")
            unlucky = True

    if unlucky:
        
        print("may as well try a level out near ", str(magic))
        
        for (a, A) in lucky_levels:
            full_obsidian_boundary = full_obsidian_boundary | A
            

#well, we've done all the computing, now its just a matter of finishing the Model, making a palette,
#and making the plan.

#air = 0
    palette = [(255, 255, 255)]

#sandstone and netherbrick = 1 and 2 or vice versa
    if red_is_1:
        palette.append((255, 0, 0))
        palette.append((255, 255, 0))
    else:
        palette.append((255, 255, 0))
        palette.append((255, 0, 0))

#obsidian = 3
    palette.append((0,0,0))

#glowstone = 4
    palette.append((255, 165, 0))

#markers = 5
    palette.append((0, 0, 255))

#shadow = 6
    palette.append((170, 170, 170))

    m.set_palette(palette)
    

    for x, y, z in full_obsidian_boundary:
        g.put(3, x, y, z)

    for x, y, z in full_glowstone_boundary:
        g.put(4, x, y, z)

    m.append_grid('b', '0')

    h = m.grids[-1]

    for x, y, z in m.i.iter_indices_z_level(-(m.i.cz)):

        value = g.get(x, y, z)

        if value != 0:
            h.put(value, x, y, z)

        elif x%5 == 0 and y%5 == 0:
            h.put(5, x, y, z)

    for z0 in range(-(m.i.cz) + 1, m.i.cz + 1):

        for x, y, z in m.i.iter_indices_z_level(z0):

            value = g.get(x, y, z)

            if value != 0:
                h.put(value, x, y, z)

            elif 0 < h.get(x, y, z-1) < 5:
                h.put(6, x, y, z)

            elif x%5 == 0 and y%5 == 0:
                h.put(5, x, y, z)

    m.make_plan()

                    
    


    

