'''
Created on Apr 5, 2012

@author: bill
'''

from itertools import starmap
from operator import mul
from math import cos, sin

def d2( vector1, vector2):
    d = 0
    for i in range(len(vector1)):
        d += (vector1[i] - vector2[i])**2
    return d

def dot(vector1, vector2):
    return sum(starmap(mul,zip(vector1,vector2)))

def cross(xxx_todo_changeme, xxx_todo_changeme1):
    (a, b, c) = xxx_todo_changeme
    (d, e, f) = xxx_todo_changeme1
    return (b*f - c*e, c*d - a*f, a*e - b*d)

def rotation(theta):
    """ returns a function that rotates a 2-vector by theta.
    """

    costheta = cos(theta)
    sintheta = sin(theta)

    def f(x, y):
        return x*costheta - y*sintheta, x*sintheta + y*costheta

    return f



def squarezaxisrotations(x, y, z):
    return [(x, y, z), (-y, x, z), (-x, -y, z), (y, -x, z)]

def squarezaxisrotationsandreflections(x, y, z):
    return squarezaxisrotations(x, y, z) + squarezaxisrotations(-x, y, z)
