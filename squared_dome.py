"""
created 9 may 2013.
Creates a blueprint for a dome built on a square building with some rounding at the corners (possibly 0).
"""
__author__ = 'bill'

import model
import math


def squared_dome(sidelength, radius2atcorners, blockcentredatcorners=True):

    dx = sidelength + 6
    dy = sidelength + 6
    dz = sidelength/2 + 7

    cx = 3
    cy = 3
    cz = 3

    if blockcentredatcorners:
        lowcentre = math.floor(math.sqrt(radius2atcorners))
    else:
        test = 0.5
        while (test + 1) ** 2 <= radius2atcorners:
            test += 1

        lowcentre = test

    highcentre = sidelength - 1 - lowcentre

    m = model.Model(dx=dx,
                    dy=dy,
                    dz=dz,
                    cx=cx,
                    cy=cy,
                    cz=cz,
                    name="squared_dome",
                    palette=[(255, 255, 255), (0, 0, 0), (85, 85, 85), (170, 170, 170), (255, 0, 0)])

    m.append_grid('b', "0")
    g = m.last_grid()

    def in_a_middle(x, y):
        return (lowcentre <= x <= highcentre) and (0<= y < sidelength) \
            or (lowcentre <= y <= highcentre) and (0<= x < sidelength)

    def near_rounded(x, y):
        return min((x-lowcentre)**2, (x-highcentre)**2) + \
               min((y-lowcentre)**2, (y-highcentre)**2) <= radius2atcorners

    midline = (sidelength - 1)/2.0

    def radius_at(x):
        if x < lowcentre:
            return math.sqrt(radius2atcorners - (lowcentre - x)**2) +\
                   midline - lowcentre

        elif lowcentre <= x <= highcentre:
            return math.sqrt(radius2atcorners) + midline - lowcentre

        elif highcentre < x:
            return math.sqrt(radius2atcorners - (highcentre - x)**2) +\
                   midline - lowcentre

    def height_at(x, y):
        return min(math.sqrt(radius_at(x)**2 - (y - midline)**2),
                   math.sqrt(radius_at(y)**2 - (x - midline)**2))

    for x,y,z0 in m.i.iter_indices_z_level(0):

        if in_a_middle(x,y) or near_rounded(x,y):
            h = height_at(x,y)
            for z in m.i.range_z():
                if z<= h:
                    g[x,y,z] = 1

    m.append_hollowed_grid()
    g = m.last_grid()
    m.append_grid('b', "0")
    h = m.last_grid()

    for z0 in m.i.range_z()[1:]:
        for (x, y, z) in m.i.iter_indices_z_level(z0):
            if g[x, y, z] == 0 and g[x, y, z-1] == 1:
                # pale grey for "empty this level, full below"
                h[x, y, z] = 3
            elif g[x, y, z] == 1 and g[x, y, z-1] == 0:
                # dark grey for "full this level, empty below"
                h[x, y, z] = 2
            elif g[x, y, z] == 1 and g[x, y, z-1] == 1:
                # black for "put a block on a block"
                h[x, y, z] = 1

    m.add_torches()
    m.make_plan()